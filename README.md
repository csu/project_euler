# [Christopher Su](http://christophersu.net)'s Project Euler Solutions
Here are my solutions to problems from [Project Euler](http://projectueler.net). I'll be attempting to complete them in as many languages as I can, but as of right now (July 11, 2013), I'm doing them in Java first.

### Todo list:
#### Attempted but not yet complete
N/A

#### Completed but code not executed
* Problem 12
* Problem 25